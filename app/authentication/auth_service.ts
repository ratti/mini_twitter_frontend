import { IUser } from "../schemas/entity_set";
import { IRequestResult } from "../schemas/structure";
import { StoreKeys, Routes } from "../helpers/config_keys"
let _ = require("underscore")

interface ILoginParams {
	email: string
	password: string
	rememberMe: boolean
}

// interface IChangePasswordParams {
// 	currentPassword: string
// 	newPssword: string
// 	confirmPassword: string
// }

interface IRegisterParams {
	name: string
	email: string
	password: string
	password_confirmation: string
}

interface IAuthService {
	isLogin(): boolean
	login(params: ILoginParams): angular.IPromise<IRequestResult<IUser>>
	// changePassword(params: IChangePasswordParams): angular.IPromise<IRequestResult<any>>
	checkLogin(): void
	logOut(): angular.IPromise<IRequestResult<any>>
	register(params: IRegisterParams): angular.IPromise<IRequestResult<IUser>>
	setUser(user: IUser): void
	currentUser: IUser
}

class AuthService implements IAuthService {
	currentUser: IUser;

	static $inject = ["$q", "$http", "$state", "BASEAPI"];

	constructor(private $q: angular.IQService,
		private $http: angular.IHttpService,
		private $state: angular.ui.IStateService,
		private baseUrl: string) {
		if (localStorage.getItem(StoreKeys.CurrentUser)) {
			this.currentUser = JSON.parse(localStorage.getItem(StoreKeys.CurrentUser))
		}
	}

	login(loginDetails: ILoginParams) {
		let defer = this.$q.defer()
		
		this.$http.post(`${this.baseUrl}/auth/sign_in`,
			loginDetails).then((response) => {
				defer.resolve(response)
			})
		return defer.promise
	}

	// changePassword(passwordDetails: IChangePasswordParams) {
	// 	let defer = this.$q.defer()
	// 	this.$http.post(`${this.baseUrl}/account/changepassword`,
	// 		passwordDetails).then((response) => {
	// 			defer.resolve(response)
	// 		})
	// 	return defer.promise
	// }

	logOut() {
		let defer = this.$q.defer()
		
		this.$http.delete(`${this.baseUrl}/auth/sign_out`).then((response: IRequestResult<any>) => {
			if (response.success) {
				localStorage.removeItem(StoreKeys.CurrentUser)
				this.currentUser = null
			}
			defer.resolve(response)
		});

		return defer.promise
	}

	register(registerDetails: IRegisterParams) {
		let defer = this.$q.defer()
		
		this.$http.post(`${this.baseUrl}/auth`, registerDetails).then((response) => {
				defer.resolve(response)
			})
		return defer.promise
	}

	checkLogin() {
		if (!this.isLogin()) { this.$state.go(Routes.Login) }
	}

	isLogin() { return !!this.currentUser }

	setUser(user: IUser) {
		this.currentUser = user
		localStorage.setItem(StoreKeys.CurrentUser, JSON.stringify(user))
	}

}

export {AuthService, IAuthService, ILoginParams, IRegisterParams}