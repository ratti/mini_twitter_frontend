import {IAuthService} from "../authentication/auth_service"
import { UserProfileService, IUserProfileService } from './user_profile_service';
import { FeedService, IFeedService } from '../main/feed_service';
import {MessageBox} from "../helpers/message_box"
let _ = require("underscore")


class UserProfileCtrl {
	isLoading: boolean;
	followings: any[];
	followers: any[];
	user_tweets: any[];
	stats: {};
	
	static $inject = ["$state", "AuthService", "UserProfileService", "FeedService"];
	
	constructor(
		private $state: angular.ui.IStateService,
		private authService: IAuthService, 
		private userProfileService: IUserProfileService,
		private feedService: IFeedService
		) {
			this.start();
		}
	
	// changePassword(params: IChangePasswordParams){
	// 	this.isLoading = true
	// 	this.authService.changePassword(params).then((res) => {
	// 		this.isLoading = false
	// 		MessageBox.display(res.message, res.success)
	// 	})
	// }

	unfollow(id: number) {
        this.feedService.unfollow(id).then((res) => {
            if (res.success) {
                // var index = _.findIndex(this.followings, (user_id: number) => (user_id === id));
                // this.followings.splice(index, 1);
				this.getFollowings();
                console.log(this.followings);
            }
        });
    }

	private getFollowings() {
        this.userProfileService.followings().then((res) => {
            if (res.success) {
                this.followings = res.data;
            }
        });
	}

	private getFollowers() {
        this.userProfileService.followers().then((res) => {
            if (res.success) {
                this.followers = res.data;
            }
        });
	}

	private getUsersTweets() {
        this.userProfileService.user_tweets().then((res) => {
            if (res.success) {
                this.user_tweets = res.data;
            }
        });
	}

	private getStats() {
        this.feedService.stats().then((res) => {
            if (res.success) {
                this.stats = res.data;
            }
        });
    }

	private start() {
		this.getFollowings();
		this.getFollowers();
		this.getUsersTweets();
		this.getStats();
	}
}

export {UserProfileCtrl}