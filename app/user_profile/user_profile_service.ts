import { IRequestResult } from "../schemas/structure";

interface IUserProfileService {
    followings(): angular.IPromise<IRequestResult<any>>
    followers(): angular.IPromise<IRequestResult<any>>
    user_tweets(): angular.IPromise<IRequestResult<any>>
}

class UserProfileService {

    static $inject = ['$q', '$http', 'BASEAPI'];
	
	constructor(private $q: angular.IQService,
		private $http: angular.IHttpService,
		private baseUrl: string) {}

    followings() {
        let defer = this.$q.defer();
        this.$http.get(`${this.baseUrl}/users/followings`)
            .then((res) => {
                defer.resolve(res);
            });
        return defer.promise;
    }

    followers() {
        let defer = this.$q.defer();
        this.$http.get(`${this.baseUrl}/users/followers`)
            .then((res) => {
                defer.resolve(res);
            });
        return defer.promise;
    }

    user_tweets() {
        let defer = this.$q.defer();
        this.$http.get(`${this.baseUrl}/tweets/user_tweets`)
            .then((res) => {
                defer.resolve(res);
            });
        return defer.promise;
    }
}

export {UserProfileService, IUserProfileService}