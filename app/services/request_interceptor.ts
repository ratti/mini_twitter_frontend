import { MessageBox, Toast } from "../helpers/message_box"
import { SysMessages, StoreKeys } from '../helpers/config_keys';
import {IUser} from "../schemas/entity_set"
let _ = require("underscore")

interface ITokenFormat {
    access_token: string;
    token_type: string;
    client: string;
    expiry: string;
    uid: string

}

class RequestInterceptor {
    private get GetMethod() { return "GET" }
    private get DeleteMethod() { return "DELETE" }
    private get QuerySuffix() { return "/query" }

    currentUser: IUser;
    isLoggedIn: boolean;
    newHeaders: ITokenFormat;

    static $inject = ["$q", "$location"];

    constructor(private $q: angular.IQService,
        private $location: angular.ILocationService) { 
            this.newHeaders = {
                access_token: "string",
                token_type: "string",
                client: "string",
                expiry: "string",
                uid: "string"
            }
        }

    private getCurrentUser = () => {
        this.currentUser = JSON.parse(localStorage.getItem(StoreKeys.CurrentUser))
        this.isLoggedIn = !!this.currentUser
    }

    private getMessage(response: any) {
        if (response.data.success) {
            if (response.data.message) return response.data.message;
            return (response.config.method === this.DeleteMethod) ? SysMessages.RecordDeleted : SysMessages.RecordSaved
        } else {
            return response.data.message || SysMessages.OperationError;
        }
    }

    private isQueryPost(url: string) {
        return url.indexOf(this.QuerySuffix, url.length - this.QuerySuffix.length) !== -1;
    }

    private updateHeaders(response: angular.IHttpPromiseCallbackArg<any>) {
        this.newHeaders.access_token = response.headers("access-token");
        this.newHeaders.token_type = response.headers("token-type");
        this.newHeaders.client = response.headers("client");
        this.newHeaders.expiry = response.headers("expiry");
        this.newHeaders.uid = response.headers("uid");
    }

    private setAuthHeaders(headers: ITokenFormat) {
        localStorage.setItem(StoreKeys.AuthHeaders, JSON.stringify(headers));
    }

    private getAuthHeaders(config: angular.IRequestConfig) {
        let headers = JSON.parse(localStorage.getItem(StoreKeys.AuthHeaders)) || {};
        console.log(headers);
        config.headers["access-token"] = headers.access_token;
        config.headers["token-type"] = headers.token_type;
        config.headers["client"] = headers.client;
        config.headers["expiry"] = headers.expiry;
        config.headers["uid"] = headers.uid;
    }

    request = (config: angular.IRequestConfig) => {
        this.getCurrentUser();
        this.getAuthHeaders(config);
        return config
    }

    requestError = (rejection: any) => {
        return rejection;
    }

    response = (response: angular.IHttpPromiseCallbackArg<any>) => {
        // if (response.status === 200 && response.config.method !== this.GetMethod
        //     && !this.isQueryPost(response.config.url)) {
        //     var message = this.getMessage(response);
        //     Toast.display(message, response.data.success);
        // }
        if (response.status === 200 || response.data.success) {
            this.updateHeaders(response);
            this.setAuthHeaders(this.newHeaders);
        }
        return (response.config.cache) ? response : this.$q.when(response.data)
    }

    responseError = (rejection: angular.IHttpPromiseCallbackArg<any>) => {
        switch (+rejection.status) {
            case 401:
                Toast.error(SysMessages.Unauthorized);
                break;
            case 404:
                Toast.error(SysMessages.NotFound);
                break;
            case -1:
                Toast.error(SysMessages.NotAllowed);
                break;
            case 502:
                Toast.error(SysMessages.BadGateway);
                break;
            default:
                Toast.error(rejection.statusText);
                break;
        }

        return rejection;
    }
}

export {RequestInterceptor}