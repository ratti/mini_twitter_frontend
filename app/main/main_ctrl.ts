import {IUser} from "../schemas/entity_set"
import {IAuthService} from "../authentication/auth_service"
import {MessageBox} from "../helpers/message_box"
import { Routes } from '../helpers/config_keys';

interface IMenuItem {
    label: string
    route: string
    icon: string
}

class MainCtrl {
    version: string;
    id: number;
    username: string;
    fullname: string;
    menuItems: Array<IMenuItem>

    static $inject = ["$q", "$rootScope", "$state", "AuthService"];

    constructor(private $q: angular.IQService,
        private $rootScope: any,
        private $state: angular.ui.IStateService,
        private authenticate: IAuthService) {
        this.setVersion()
        this.$rootScope.$state = this.$state;
    }

    isLoggedIn() {
        this.setUserName()
        return this.authenticate.isLogin();
    }

    signOut() {
        this.authenticate.logOut().then((res) => {
            if (res.success) {
                this.$state.go(Routes.Login)
            }
        })
    }

    setVersion() {
        this.version = "0.0.0"
    }

    setUserName() {
        let user = this.authenticate.currentUser
        this.username = user ? user.username : "";
        this.fullname = user ? user.name : "";
        this.id = user ? user.id : null;
    }
}

export {MainCtrl}