import { IRequestResult } from "../schemas/structure";

interface IFeedService {
    get(): angular.IPromise<IRequestResult<any>>
    save(tweet: string): angular.IPromise<IRequestResult<any>>
    stats(): angular.IPromise<IRequestResult<any>>
    users(): angular.IPromise<IRequestResult<any>>
    follow(id: number): angular.IPromise<IRequestResult<any>>
    unfollow(id: number): angular.IPromise<IRequestResult<any>>
}

class FeedService {

    static $inject = ['$q', '$http', 'BASEAPI'];
	
	constructor(private $q: angular.IQService,
		private $http: angular.IHttpService,
		private baseUrl: string) {}

    get() {
        let defer = this.$q.defer();
        this.$http.get(`${this.baseUrl}/tweets`)
            .then((res) => {
                defer.resolve(res);
            });
        return defer.promise;
    }

    save(tweet: string) {
        let defer = this.$q.defer();
        this.$http.post(`${this.baseUrl}/tweets`, {'tweet': {'content': tweet}})
            .then((res) => {
                defer.resolve(res);
            });
        return defer.promise;
    }

    stats() {
        let defer = this.$q.defer();
        this.$http.get(`${this.baseUrl}/users/stats`)
            .then((res) => {
                defer.resolve(res);
            });
        return defer.promise;
    }

    users() {
        let defer = this.$q.defer();
        this.$http.get(`${this.baseUrl}/users`)
            .then((res) => {
                defer.resolve(res);
            });
        return defer.promise;
    }

    follow(id: number) {
        let defer = this.$q.defer();
        this.$http.post(`${this.baseUrl}/relationships`, {following_id: id})
            .then((res) => {
                defer.resolve(res);
            });
        return defer.promise;
    }

    unfollow(id: number) {
        let defer = this.$q.defer();
        this.$http.delete(`${this.baseUrl}/relationships/${id}`)
            .then((res) => {
                defer.resolve(res);
            });
        return defer.promise;
    }
}

export {FeedService, IFeedService}