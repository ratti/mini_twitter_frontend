import { FeedService, IFeedService } from './feed_service';
let _ = require("underscore");

class FeedCtrl {

    saving: boolean;
    isLoading: boolean;
    records: any[];
    record: string;
    stats: {};
    users: any[];
    isFollowing: boolean;
    
    static $inject = ["FeedService"]

    constructor(private feedService:IFeedService) {
        this.start();
    }

    saveTweet(tweet: string) {
        console.log(tweet)
        this.saving = true;
        this.feedService.save(tweet).then((res) => {
            this.saving = false;
            if (res.success) {
                this.record = "";
                this.records.unshift(res.data);
                this.getStats();
            }
        });
    }

    fetchTweets() {
        this.isLoading = true;
        this.feedService.get().then((res) => {
            this.isLoading = false;
            if (res.success) {
                this.records = res.data;
            }
        });
    }

    follow(id: number) {
        this.feedService.follow(id).then((res) => {
            if (res.success) {
                // this.isFollowing = res.data;
                // var index = _.findIndex(this.users, (user_id: number) => (user_id === id));
                // this.users.splice(index, 1);
                this.fetchUsers();
                console.log(this.users);
            }
        });
    }

    private getStats() {
        this.feedService.stats().then((res) => {
            if (res.success) {
                this.stats = res.data;
            }
        });
    }

    private fetchUsers() {
        this.feedService.users().then((res) => {
            if (res.success) {
                this.users = res.data;
            }
        });
    }

    private start() {
        this.getStats();
        this.fetchUsers();
        this.fetchTweets();
    }
}

export {FeedCtrl}