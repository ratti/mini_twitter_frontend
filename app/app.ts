/// <reference path="../typings/tsd.d.ts" />

import {Routes} from './helpers/config_keys';
import {AppRoutes} from "./app_routes"
import {RequestInterceptor} from './services/request_interceptor';
import {MainCtrl} from "./main/main_ctrl"
import {FeedCtrl} from "./main/feed_ctrl"
import {FeedService} from './main/feed_service';
import {LoginCtrl} from "./authentication/login_ctrl"
import {UserProfileCtrl} from "./user_profile/user_profile_ctrl"
import {UserProfileService} from "./user_profile/user_profile_service"
import {AuthService, IAuthService} from "./authentication/auth_service"

let app = angular.module('app', 
	['ui.router', 
	 'ui.bootstrap', 
	 'ui.select2', 
	 'ngAnimate', 
	 'ngSanitize', 
	 'alcoder.components'])
	
    .controller('LoginCtrl', LoginCtrl)
	.controller('UserProfileCtrl', UserProfileCtrl)
	.controller('MainCtrl', MainCtrl)
	.controller('FeedCtrl', FeedCtrl)
	.service("RequestInterceptor", RequestInterceptor)
	.service('AuthService', AuthService)
	.service('FeedService', FeedService)
	.service('UserProfileService', UserProfileService)


app.config(AppRoutes);

app.value("BASEAPI", "api");

app.run(($rootScope: angular.IRootScopeService, $state: angular.ui.IStateService, AuthService: IAuthService) => {
	$rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
		// Check if user is not logged in
		if (!AuthService.isLogin()) {
			// Remain on the login page
			$state.transitionTo(Routes.Login);
			event.preventDefault();
		}
	})
});

export {app}