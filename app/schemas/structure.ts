interface IRequestResult<T> {
	data: T
	success: boolean
	message: string
	total: number
}

export {IRequestResult}