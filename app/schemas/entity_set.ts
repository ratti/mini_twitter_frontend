interface IhasId {
	id: number
}

interface IAuditFields extends IhasId {
	createdAt: Date
	createdBy: string
	modifiedAt: Date
	modifiedBy: string
}

interface IUser {
	id: number
	email: string
	username: string
	name: string
	provider: string
	uid: string
}


export {IUser}