import { Routes } from './helpers/config_keys';

let AppRoutes = ($stateProvider: any,
    $urlRouterProvider: angular.ui.IUrlRouterProvider,
    $httpProvider: angular.IHttpProvider) => {
    $stateProvider
        .state(Routes.Login, {
            url: '/login',
            template: require('./authentication/login.html'),
            controller: 'LoginCtrl',
            controllerAs: 'loginVm'
        })
        // .state(Routes.ChangePassword, {
        //     url: '/changePassword',
        //     template: require('./user_profile/change_password.html'),
        //     controller: 'UserProfileCtrl',
        //     controllerAs: 'profileVm'
        // })
        .state(Routes.Feed, {
            url: '/feed',
            template: require('./main/feed.html'),
            controller: 'FeedCtrl',
            controllerAs: 'feedVm'
        })
        .state(Routes.Following, {
            url: '/following',
            template: require('./user_profile/following.html'),
            controller: 'UserProfileCtrl',
            controllerAs: 'profileVm'
        })
        .state(Routes.Followers, {
            url: '/followers',
            template: require('./user_profile/followers.html'),
            controller: 'UserProfileCtrl',
            controllerAs: 'profileVm'
        })
        .state(Routes.Profile, {
            url: '/profile',
            template: require('./user_profile/profile.html'),
            controller: 'UserProfileCtrl',
            controllerAs: 'profileVm'
        })

    $urlRouterProvider.otherwise("feed");
    $httpProvider.interceptors.push("RequestInterceptor")
}

export {AppRoutes}