# Mini Twitter
This is a twitter-like application. Built on angular-typscript-webpack seed application by Albert Adjetey

## Libraries
+ Angular
+ Webpack
+ Typescript
+ Bootstrap
+ FontAwesome
+ Roboto Font (local)

## To run
+ `npm install`
+ `npm start`. This will start the app on port 5000
+ You can then access the app with this url `http://localhost:5000`
